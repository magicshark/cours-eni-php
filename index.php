<?php
//Autoloading
session_start();
require_once 'kernel/utils/AutoloaderUtils.php';
\Core\Utils\Autoloader::register();

//Router
if (isset($_GET['p'], $_GET['a'])) {
    $router = new Core\Route\RouterUtils($_GET['p'], $_GET['a']);
} else {
    $router = new Core\Route\RouterUtils();
}
$session = new \Core\Session\SessionServiceUtils();
$flash = new \Core\FlashMessage\MessageUtils($session);
$controleur = $router->controller;

//Formulaire
$form = new \Core\Form\FormUtils($_POST);

//Html
require_once './kernel/vue/header.php';
echo '<div class="container">';
$vu = $router->routes[$router->route]['vue'];
if ($vu != "") {
    require_once $vu;
} else {
    if ($router->route == "biere.supprimer") {
        $controleur->supprimerBiere($flash);
    }elseif ($router->route == "categorie.supprimer") {
        $controleur->supprimerCategorie($flash);
    }
}
echo "</div>";
require_once './kernel/vue/footer.php';
?>