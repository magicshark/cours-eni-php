<?php

namespace Bean;

class BiereBean {

    private $id;
    private $marque;
    private $degres;
    
    /**
     *
     * @var type \Bean\CategorieBean
     */
    private $categorie;

    public function __construct($marque = null, $degres = null){
        $this->marque = $marque;
        $this->degres = $degres;
    }
    
    public function __get($attr) {
        return $this->$attr;
    }

    public function __set($attr, $value) {
        $this->$attr = $value;
    }

}
