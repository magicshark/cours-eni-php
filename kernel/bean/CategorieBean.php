<?php


namespace Bean;

class CategorieBean {
    
    private $id;
    private $libelle;
    
    public function __construct($libelle = null){
        $this->libelle = $libelle;
    }
    
    public function __get($attr) {
        return $this->$attr;
    }

    public function __set($attr, $value) {
        $this->$attr = $value;
    }
       
   
}
