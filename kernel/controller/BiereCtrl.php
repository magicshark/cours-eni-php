<?php
namespace Controller;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of biere
 *
 * @author externe
 */
class BiereCtrl {

    private $bierreMDL;

    public function __construct() {
        $this->bierreMDL = new \Model\BiereMdl();
    }

    public function ajouterBiere(\Core\FlashMessage\MessageUtils $flash) {
        if (
                isset($_POST["submit"], $_POST["marque"], $_POST["degres"]) && !empty($_POST["marque"]) && (!empty($_POST["degres"]) || $_POST['degres'] === 0)
                && \is_numeric($_POST['degres'])
              ) {
            $biere = new \Bean\BiereBean($_POST["marque"], $_POST["degres"]);
            $biere->categorie = new \Bean\CategorieBean();
            $biere->categorie->id = $_POST["categorie"];
            if ($this->bierreMDL->ajouterBiere($biere)) {
                $flash->success("La bière $biere->marque a bien été ajoutée");
                header("location:./?p=biere&a=afficher");
                die();
            }else {
                $flash->error("Une erreur est survenu");
            }
        }elseif($_SERVER['REQUEST_METHOD'] === 'POST'){
            $flash->error("Merci de vérifier les champs de saisies");
        }
    }

    public function lireBieres(): array {
        return $this->bierreMDL->getAllBiere();
    }

    public function supprimerBiere(\Core\FlashMessage\MessageUtils $flash) {
        $biere = new \Bean\BiereBean($_POST["marque"], $_POST["degres"]);
        $biere->id = $_POST["id"];
        if($this->bierreMDL->supprimerBiere($biere)){
            $flash->success("La bière a bien été supprimée");
        }else{
            $flash->error("la bière n'a pas pu être supprimée");
        }
        header("location:./?p=biere&a=afficher");
        die();
    }

    public function modifierBiere(\Core\FlashMessage\MessageUtils $flash) {
        if (
                isset($_POST["submit"], $_POST["marque"], $_POST["degres"]) && !empty($_POST["marque"]) && (!empty($_POST["degres"]) || $_POST['degres'] === 0)
        ) {
            $biere = new \Bean\BiereBean($_POST["marque"], $_POST["degres"]);
            $biere->id = $_POST["id"] ;
            $biere->categorie = new \Bean\CategorieBean();
            $biere->categorie->id = $_POST["categorie"];
            if($this->bierreMDL->modifierBiere($biere)) {
                $flash->success("La bière " . $biere->marque. " a bien été modifiée");
            }else{
                $flash->error("la bière n'a pas été modifiée");
            }
            header("location:./?p=biere&a=afficher");
            die();
        }
    }

    public function lireBiere() {
        $biere = $this->bierreMDL->getOne($_GET['id']);
        if ($biere) {
            return $biere;
        }
        header("location:./?p=biere&a=afficher");
        die();
    }

}
