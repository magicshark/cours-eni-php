<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

/**
 * Description of CategorieCtrl
 *
 * @author Administrateur
 */
class CategorieCtrl {
    
    private $categorieMDL;

    public function __construct() {
        $this->categorieMDL = new \Model\CategorieMdl();
    }

    public function ajouterCategorie(\Core\FlashMessage\MessageUtils $flash) {
        if (
                isset($_POST["submit"], $_POST["libelle"]) && !empty($_POST["libelle"]))
         {
            $categorie = new \Bean\categorieBean($_POST["libelle"]);
            if($this->categorieMDL->ajouterCategorie($categorie)) {
                $flash->success("La catégorie $categorie->libelle à bien été ajoutée");
            }else {
                $flash->error("la categorie $categorie->libelle n'a pas été ajoutée");
            }
            header("location:./?p=categorie&a=afficher");
            die();
        }
    }

    public function lireTouteCategorie(): array {
        return $this->categorieMDL->getAllCategories();
    }

    public function supprimerCategorie(\Core\FlashMessage\MessageUtils $flash) {
        $categorie = new \Bean\categorieBean($_POST["libelle"]);
        $categorie->id = $_POST["id"];
        if($this->categorieMDL->supprimerCategorie($categorie)) {
            $flash->success("La catégorie $categorie->libelle à bien été supprimée");
        }else {
            $flash->error("la categorie $categorie->libelle n'a pas été supprimée");
        }
        header("location:./?p=categorie&a=afficher");
        die();
    }

    public function modifierCategorie(\Core\FlashMessage\MessageUtils $flash) {
        if (
            isset($_POST["submit"], $_POST["libelle"]) && !empty($_POST["libelle"])) {
            $categorie = new \Bean\categorieBean($_POST["libelle"]);
            $categorie->id = $_POST["id"] ;
            if($this->categorieMDL->modifierCategorie($categorie)) {
                $flash->success("La catégorie $categorie->libelle à bien été modifiée");
            }else {
                $flash->error("la categorie $categorie->libelle n'a pas été modifiée");
            }
            header("location:./?p=categorie&a=afficher");
            die();
        }
    }

    public function lireCategorie() {
        $categorie = $this->categorieMDL->getOne($_GET['id']);
        if ($categorie) {
            return $categorie;
        }
        header("location:./?p=categorie&a=afficher");
        die();
    }
}
