<?php

namespace Controller;

class UserCtrl {

    private $userMDL;

    public function __construct() {
        $this->userMDL = new \Model\UserMdl();
    }

    public function ajouterUser() {
        if (
                isset($_POST["submit"], $_POST["pseudo"], $_POST["password"], $_POST["passwordValidation"]) 
                && !empty($_POST["passwordValidation"]) 
                && !empty($_POST["password"]) 
                && !empty($_POST["pseudo"]) 
                && strlen($_POST["password"]) >= 8 
                && strlen($_POST["pseudo"]) >= 5
        ) {
            if ($_POST["password"] != $_POST["passwordValidation"]) {
                return;
            }
            $user = new \Bean\UserBean();
            $user->pseudo = $_POST["pseudo"];
            $user->password = password_hash($_POST["password"], PASSWORD_DEFAULT);
            $this->userMDL->ajouterUser($user);
            header("location:?p=biere&a=afficher");
            die();
        } 
    }

    public function connecter() {
        if (
                isset($_POST["submit"], $_POST["pseudo"], $_POST["password"]) 
                && !empty($_POST["password"]) 
                && !empty($_POST["pseudo"]) 
                && strlen($_POST["password"]) >= 8 
                && strlen($_POST["pseudo"]) >= 5
        ) {
            $user = new \Bean\UserBean();
            $user->pseudo = $_POST["pseudo"];
            $user->password = password_hash($_POST["password"], PASSWORD_DEFAULT);
            $userRes = $this->userMDL->lireUserByPseudo($user);
            if ($userRes != null && password_verify($_POST["password"], $userRes->password)) {
                $_SESSION["user"] = $userRes;
                header("location:?p=biere&a=afficher");
                die();
            }
        }
    }
}
