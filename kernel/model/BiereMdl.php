<?php

namespace Model;

class BiereMdl extends \Core\Bdd\CrudUtils
{

    protected $table = "biere";

    public function __construct()
    {
        parent::__construct();
        $this->connection = \Core\Bdd\ConnectionUtils::getInstance();
    }

    public function getAllBiere(): array
    {
        $bieres = parent::getAll(["foreign"=>[
                "table"=>"categorie",
                "champ"=>"id",
                "table2"=>$this->table,
                "champ2"=>"categorie"
                ],
            ]);
        $return = [];
        foreach ($bieres as $biere) {
            $biereEncours = new \Bean\BiereBean();
            $biereEncours->categorie = new \Bean\CategorieBean();
            foreach ($biere as $column => $value) {
                if ($column == "categorie") {
                    $biereEncours->categorie->id = $value;
                } elseif ($column == "libelle") {
                    $biereEncours->categorie->libelle = $value;
                } else {
                    $biereEncours->$column = $value;
                }
            }
            $return[] = $biereEncours;
        }
        return $return;
    }

    public function getOne($id): \Bean\BiereBean
    {
        $bieres = parent::getOne($id);
        $return = false;
        foreach ($bieres as $biere) {
            $biereEncours = new \Bean\BiereBean();
            $biereEncours->categorie = new \Bean\CategorieBean();
            foreach ($biere as $column => $value) {
                if ($column == "categorie") {
                    $biereEncours->categorie->id = $value;
                } elseif ($column == "libelle") {
                    $biereEncours->categorie->libelle = $value;
                } else {
                    $biereEncours->$column = $value;
                }
            }
            $return = $biereEncours;
        }
        return $return;
    }

    public function ajouterBiere(\Bean\BiereBean $biere): \Bean\BiereBean
    {
        $elmt = ["marque" => $biere->marque, "degres" => $biere->degres, "categorie" =>$biere->categorie->id];
        parent::insert($elmt);
        $biere->id = parent::lastInsert();
        return $biere;
    }

    public function supprimerBiere(\Bean\BiereBean $biere)
    {
        parent::delete(["marque" => $biere->marque, "degres" => $biere->degres, "id" => $biere->id]);
        return count(parent::getOne($biere->id)) === 0;
    }

    public function modifierBiere(\Bean\BiereBean $biere)
    {
        return parent::update(["marque" => $biere->marque, "degres" => $biere->degres, "categorie" =>$biere->categorie->id], $biere->id);
    }
}
