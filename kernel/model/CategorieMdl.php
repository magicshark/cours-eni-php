<?php
namespace Model;
class CategorieMdl extends \Core\Bdd\CrudUtils{
    protected $table = "categorie";
    
    public function __construct(){
        parent::__construct();
        $this->connection = \Core\Bdd\ConnectionUtils::getInstance();
    }
    
    public function getAllCategories(): array{
        $categories = parent::getAll();
        $return = [];
        foreach ($categories as $categorie) {
            $categorieEncours = new \Bean\CategorieBean();
            foreach($categorie as $column => $value) {
                $categorieEncours->$column = $value; 
            }
            $return[] = $categorieEncours;
        }
        return $return;
    }
      public function getOne($id): \Bean\CategorieBean {
        $categories = parent::getOne($id);
        $return = false;
        foreach ($categories as $categorie) {
            $categorieEncours = new \Bean\CategorieBean();
            foreach ($categorie as $column => $value) {
                $categorieEncours->$column = $value;
            }
            $return = $categorieEncours;
        }
        return $return;
    }

    public function ajouterCategorie(\Bean\CategorieBean $categorie){
        $elmt = ["libelle" => $categorie->libelle];
        return parent::insert($elmt);
    }

    public function supprimerCategorie(\Bean\CategorieBean $categorie) {
        return parent::delete(["libelle" => $categorie->libelle, "id" => $categorie->id]);
    }

    public function modifierCategorie(\Bean\CategorieBean $categorie) {
        return parent::update(["libelle" => $categorie->libelle], $categorie->id);
    }
}
