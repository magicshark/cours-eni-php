<?php

namespace Model;

class UserMdl extends \Core\Bdd\CrudUtils {

    protected $table = "user";

    public function __construct() {
        parent::__construct();
        $this->connection = \Core\Bdd\ConnectionUtils::getInstance();
    }

    public function getAllUser(): array {
        $users = parent::getAll();
        $return = [];
        foreach ($users as $user) {
            $userEncours = new \Bean\UserBean();
            foreach ($user as $column => $value) {
                $userEncours->$column = $value;
            }
            $return[] = $userEncours;
        }
        return $return;
    }

    public function getOne($id): \Bean\UserBean {
        $users = parent::getOne($id);
        $return = false;
        foreach ($users as $user) {
            $userEncours = new \Bean\UserBean();
            foreach ($user as $column => $value) {
                $userEncours->$column = $value;
            }
            $return = $userEncours;
        }
        return $return;
    }
    
    public function lireUserByPseudo(\Bean\UserBean $user) {
        $users = parent::findBy(["pseudo" => $user->pseudo]);
        $return = null;
        foreach ($users as $user) {
            $userEncours = new \Bean\UserBean();
            foreach ($user as $column => $value) {
                $userEncours->$column = $value;
            }
            $return = $userEncours;
        }
        return $return;
    }

    public function ajouterUser(\Bean\UserBean $user): \Bean\UserBean {
        $elmt = ["pseudo" => $user->pseudo, "password" => $user->password];
        parent::insert($elmt);
        $user->id = parent::lastInsert();
        return $user;
    }

    public function modifierUser(\Bean\UserBean $user) {
        parent::update(["pseudo" => $user->pseudo, "password" => $user->password], $user->id);
    }

}
