<?php
namespace Core\Utils;
require_once 'kernel/utils/StringUtils.php';
class Autoloader{

    static function register(){
        spl_autoload_register(array('\\Core\\Utils\\Autoloader', 'autoload'));
    }

    static function autoload($class){
        $pos = strrpos($class, "\\");
        $className = substr($class, $pos + 1);
        $chemins = array(
            "Bean" => 'kernel/bean/' . $className . '.php',
            "Ctrl" => 'kernel/controller/' . $className . '.php',
            "Mdl" => 'kernel/model/' . $className . '.php',
            "Utils" => 'kernel/utils/' . $className . '.php'
        );
        foreach ($chemins as $petitMot => $chemin){
            if(StringUtils::endsWith($className, $petitMot)){
                require_once $chemin;
                break;
            }
        }

    }

}
?>