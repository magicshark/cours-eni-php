<?php
namespace Core\Bdd;
class ConnectionUtils {
    public static $instance = null;
    
    
    private $source ="mysql:host=localhost;dbname=eni";
    private $utilisateur = "root";
    private $mdp = "";
    
    
    private $pdo;
    
    static function getInstance():ConnectionUtils{
        if(self::$instance == null){
            self::$instance = new ConnectionUtils();
        }
        return self::$instance;
    }
    
    private function __construct(){
        
    }
    
    public function getConnection(): \PDO{
        if(!$this->pdo instanceof \PDO){
            $this->connect();
        }
        return $this->pdo;
    }
    
    private function connect(){
        $db = new \PDO($this->source, $this->utilisateur, $this->mdp);
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo = $db;
    }
}
