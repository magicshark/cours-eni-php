<?php

namespace Core\Bdd;

abstract class CrudUtils
{

    protected $table;

    /**
     *
     * @var type Core\Bdd\Connection
     */
    protected $connection;

    protected function __construct()
    {
    }

    protected function getAll(array $options = [])
    {
        $from = $this->table;
        if (count($options)) {
            foreach ($options as $key => $value) {
                if ($key == "foreign") {
                    $from = $value["table"] . " INNER JOIN " . $value["table2"] . " ON " . $value["table"] . "." . $value["champ"] . "=" . $value["table2"] . "." . $value["champ2"] . " ";
                }
            }
        }
        $query = "SELECT * from " . $from;
        return $this->connection
                        ->getConnection()
                        ->query($query)
                        ->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function getOne($id)
    {
        $query = "SELECT * from $this->table WHERE id='$id'";
        return $this->connection
                        ->getConnection()
                        ->query($query)
                        ->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function findBy(array $elmt)
    {
        $where = "";
        foreach (array_keys($elmt) as $col) {
            $where .= " " . $col . "=:" . $col . " AND";
        }
        $where = substr($where, 0, -4);
        $query = "SELECT * from $this->table WHERE" . $where;
        $sth = $this->connection
                ->getConnection()
                ->prepare($query);
        $sth->execute($elmt);
        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function insert(array $elmt)
    {
        $column = "";
        $values = "";
        foreach (array_keys($elmt) as $col) {
            $column .= $col . ",";
            $values .= ":" . $col . ",";
        }
        $column = substr($column, 0, -1);
        $values = substr($values, 0, -1);
        $query = "INSERT into $this->table (" . $column . ") VALUES (" . $values . ")";
        $stm = $this->connection
                ->getConnection()
                ->prepare($query);
        return $stm->execute($elmt);
    }

    protected function delete(array $elmt)
    {
        $where = "";
        foreach (array_keys($elmt) as $col) {
            $where .= " " . $col . "=:" . $col . " AND";
        }
        $where = substr($where, 0, -4);
        $query = "DELETE from $this->table WHERE" . $where;
        $stm = $this->connection
                ->getConnection()
                ->prepare($query);
        return $stm->execute($elmt);
    }

    protected function update(array $elmt, $id)
    {
        $set = "";
        foreach (array_keys($elmt) as $col) {
            $set .= " " . $col . "= :" . $col . ",";
        }
        $set = substr($set, 0, -1);
        $query = "UPDATE $this->table SET " . $set . " WHERE id=:id";
        $elmt["id"] = $id;
        $stm = $this->connection
                ->getConnection()
                ->prepare($query);
        return $stm->execute($elmt);
    }

    protected function lastInsert()
    {
        return $this->connection
                        ->getConnection()
                        ->lastInsertId();
    }
}
