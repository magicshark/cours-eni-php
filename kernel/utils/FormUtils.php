<?Php
namespace Core\Form;
class FormUtils {

    private $data;
    public $surround = "div";

    public function __construct(array $data = array()) {
        $this->data = $data;
    }

    public function input($name, $label, $options = []) {
        if (!key_exists("data", $options) && key_exists("type", $options) && $options["type"] == "radio") {
            return "";
        }
        $padLabel = key_exists("padLabel", $options) ? $options["padLabel"] : "left";

        $value = key_exists($name, $this->data) ? "value='{$this->data[$name]}'" : "";
        
        $value = key_exists("value", $options) ? "value='{$options["value"]}'" : $value;

        $type = key_exists("type", $options) ? $options["type"] : "text";

        $attr = (key_exists($name, $this->data) && $this->data[$name] == "on") ? "checked='checked'" : "";

        $attr .= (key_exists("classes", $options)) ? " class='" . $options["classes"] . "'" : "";

        $attr .= (key_exists("id", $options)) ? " id='" . $options["id"] . "'" : "";

        if ($type == "radio") {
            $value = "value='{$options["data"]}'";
            if (key_exists($name, $this->data) && $options["data"] == $this->data[$name]) {
                $attr = "checked='checked'";
            }
        }

        $return = "<input $attr $value type='$type' name='$name' />";
        if (!key_exists("noLabel", $options)) {
            if ($padLabel == "left") {
                $return = "<label>$label" . $return . "</label>";
            } else {
                $return = "<label>" . $return . "$label</label>";
            }
        }
        return $this->surround($return);
    }

    public function select($name, $label, $options) {
        $selected = "";
        $selectedId="";
        
        if (!key_exists("data", $options)) {
            return "";
        }
        if(key_exists("selected", $options)) {
            $selected = "selected";
            $selectedId=$options["selected"];
        }
        
        $select = "<label>$label<select name='$name' >";
        $opt = "";
        foreach ($options["data"] as $key => $value) {
            $opt .= "<option ". ($key==$selectedId?$selected:"") ." value='$key'>$value</option>";
        }
        $select .= $opt . "</select></label>";
        return $this->surround($select);
    }

    public function submit($name, $label) {
        return $this->surround("<button name='$name' type='submit' >$label</button>");
    }

    private function surround($html) {
        return "<{$this->surround}>$html</{$this->surround}>";
    }

}

?>