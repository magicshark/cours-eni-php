<?PHP
namespace Core\FlashMessage;

class MessageUtils{

    /**
     *@var SessionInterface
     */
    private $sessionService;

    private $sessionKey = "flash";
    private $messages = null;

    public function __construct(\Core\Session\SessionInterfaceUtils $sessionService){
        $this->sessionService = $sessionService;
    }

    public function success(String $message) {
        $flash = $this->sessionService->get($this->sessionKey, []);
        $flash["success"] = $message;
        $this->sessionService->set($this->sessionKey, $flash);
    }
    public function error(String $message) {
        $flash = $this->sessionService->get($this->sessionKey, []);
        $flash["error"] = $message;
        $this->sessionService->set($this->sessionKey, $flash);
    }
    public function warning(String $message) {
        $flash = $this->sessionService->get($this->sessionKey, []);
        $flash["warning"] = $message;
        $this->sessionService->set($this->sessionKey, $flash);
    }

    public function get(string $type) {
        if(is_null($this->messages)){
            $this->messages = $this->sessionService->get($this->sessionKey, []);
            $this->sessionService->delete($this->sessionKey);
        }
        if(array_key_exists($type,$this->messages)){
            return $this->messages[$type];
        }
        return null;
    }
}
?>