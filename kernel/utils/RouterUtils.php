<?php
namespace Core\Route;

class RouterUtils {

    private $routes = [
        "biere.view" => [
            "vue" => 'kernel/vue/biereLire.php',
            "controller" => "kernel/controller/BiereCtrl.php",
            "controllerClass" => "Controller\\BiereCtrl",
            "page" => "biere",
            "action" => "afficher",
        ],
        "biere.ajouter" => [
            "vue" => 'kernel/vue/biereAjouter.php',
            "controller" => "kernel/controller/BiereCtrl.php",
            "controllerClass" => "Controller\\BiereCtrl",
            "page" => "biere",
            "action" => "ajouter",
        ],
        "biere.supprimer" => [
            "controller" => "kernel/controller/BiereCtrl.php",
            "controllerClass" => "Controller\\BiereCtrl",
            "vue"=>"",
            "page" => "biere",
            "action" => "supprimer",
        ],
        "biere.modifier" => [
            "vue" => 'kernel/vue/biereModifier.php',
            "controller" => "kernel/controller/BiereCtrl.php",
            "controllerClass" => "Controller\\BiereCtrl",
            "page" => "biere",
            "action" => "modifier",
        ],
        "user.connexion" => [
            "vue" => 'kernel/vue/userConnexion.php',
            "controller" => "kernel/controller/UserCtrl.php",
            "controllerClass" => "Controller\\UserCtrl",
            "page" => "user",
            "action" => "connexion",
        ],
        "user.inscription" => [
            "vue" => 'kernel/vue/userAjout.php',
            "controller" => "kernel/controller/UserCtrl.php",
            "controllerClass" => "Controller\\UserCtrl",
            "page" => "user",
            "action" => "inscription",
        ],
        "categorie.view" => [
            "vue" => 'kernel/vue/categorieLire.php',
            "controller" => "kernel/controller/CategorieCtrl.php",
            "controllerClass" => "Controller\\CategorieCtrl",
            "page" => "categorie",
            "action" => "afficher",
        ],
        "categorie.ajouter" => [
            "vue" => 'kernel/vue/CategorieAjouter.php',
            "controller" => "kernel/controller/CategorieCtrl.php",
            "controllerClass" => "Controller\\CategorieCtrl",
            "page" => "categorie",
            "action" => "ajouter",
        ],
        "categorie.supprimer" => [
            "controller" => "kernel/controller/CategorieCtrl.php",
            "controllerClass" => "Controller\\CategorieCtrl",
            "vue"=>"",
            "page" => "categorie",
            "action" => "supprimer",
        ],
        "categorie.modifier" => [
            "vue" => 'kernel/vue/categorieModifier.php',
            "controller" => "kernel/controller/CategorieCtrl.php",
            "controllerClass" => "Controller\\CategorieCtrl",
            "page" => "categorie",
            "action" => "modifier",
        ],
        "defaut" => "biere.view"
    ];
    private $controller;
    private $route = null;

    public function __construct($page = "", $action = "") {
        if ($page != "" && $action != "") {
            foreach ($this->routes as $key => $route) {
                if (is_array($route) && $route["page"] == $page && $route["action"] == $action) {
                    $this->route = $key;
                    break;
                }                
            }
        }
        if (is_null($this->route)) {
            $this->route = $this->routes["defaut"];
        }
        $this->controller = new $this->routes[$this->route]["controllerClass"]();
    }

    public function __get($attr) {
        return $this->$attr;
    }

    public function generateURL($route=null) {
        if(\is_null($route)){
            return $this->generateURL($this->routes["defaut"]);
        }
        return "?p=" . $this->routes[$route]["page"] . "&a=" . $this->routes[$route]["action"];
    }

    public function is_actual($route){
        return $this->route==$route;
    }

    public function getActual() {
        return $this->route;
    }
}
