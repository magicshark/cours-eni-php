<?php
namespace Core\Session;

class SessionServiceUtils implements SessionInterfaceUtils {

    private function ensureStarted(){
        if(session_status() === PHP_SESSION_NONE ) {
            session_start();
        }
    }

    public function get(string $key,$defaut = null){
        $this->ensureStarted();
        if(array_key_exists($key,$_SESSION)){
            return $_SESSION[$key];
        }
        return $defaut;
    }
    public function set(string $key, $value){
        $this->ensureStarted();
        $_SESSION[$key] = $value;
    }

    public function delete(string $key){
        $this->ensureStarted();
        unset($_SESSION[$key]);
    }

}