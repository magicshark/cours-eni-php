<?php
Namespace Core\Utils;
class StringUtils {
    
    static function endsWith($string, $recherche)
    {
        $length = strlen($recherche);

        return $length === 0 || 
        (substr($string, -$length) === $recherche);
    }
}
