<?php
$controleur->ajouterBiere($flash);
$catControleur = new \Controller\CategorieCtrl();
$cats = $catControleur->lireTouteCategorie();
$selectData = [];
foreach ($cats as $cat) {
    $selectData[$cat->id] = $cat->libelle;
}
?>
<form action="<?= $router->generateURL("biere.ajouter"); ?>" method="post">
    <?Php
    echo $form->input("marque", "Marque :");
    echo $form->input("degres", "Degrés :");
    echo $form->select("categorie","Catégorie :",["data"=>$selectData]);
    echo $form->submit("submit", "Valider");
    ?>
</form>