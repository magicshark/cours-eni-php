<?Php
$bieres = $controleur->lireBieres();
$form->surround = "span";
?>
<div class="container">
<?php

foreach ($bieres as $biere) {
    ?>
    <ul>
        <li>
            <?= $biere->marque ?> de type <?=$biere->categorie->libelle ?> pour <?= $biere->degres ?>° d'alcool.
            <form style="display:inline" action="<?= $router->generateURL("biere.modifier"); ?>&id=<?= $biere->id ?>" method="post">
                <?= $form->submit("submit", "Modifier"); ?>
            </form>
            <form style="display:inline" action="<?= $router->generateURL("biere.supprimer"); ?>" method="post">
                <?= $form->input("marque", "", ["type" => "hidden", "value" => $biere->marque, "noLabel" => true]); ?>
                <?= $form->input("degres", "", ["type" => "hidden", "value" => $biere->degres, "noLabel" => true]); ?>
                <?= $form->input("id", "", ["type" => "hidden", "value" => $biere->id, "noLabel" => true]); ?>
                <?= $form->submit("submit", "Supprimer"); ?>
            </form>            
        </li>
    </ul>
    <?Php
}
?>
<hr />

<?php
$form->surround = "div";
require_once $router->routes["biere.ajouter"]["vue"];
?>
</div>