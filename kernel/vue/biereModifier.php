<?Php
$controleur->modifierBiere($flash);
$biere = $controleur->lireBiere();
$catControleur = new \Controller\CategorieCtrl();
$cats = $catControleur->lireTouteCategorie();
$selectData = [];
foreach ($cats as $cat) {
    $selectData[$cat->id] = $cat->libelle;
}
?>
<form action="<?= $router->generateURL("biere.modifier"); ?>&id=<?= $biere->id ?>" method="post">
    <?= $form->input("marque", "Marque : ", ["value" => $biere->marque]); ?>
    <?= $form->input("degres", "Degrés :", ["value" => $biere->degres]); ?>
    <?= $form->input("id", "", ["type" => "hidden", "value" => $biere->id, "noLabel" => true]); ?>
    <?= $form->select("categorie","Catégorie :",["data"=>$selectData,"selected"=>$biere->categorie->id]);?>
    <?= $form->submit("submit", "Modifier"); ?>
</form>