<?Php
$categories = $controleur->lireTouteCategorie();
$form->surround = "span";
foreach ($categories as $categorie) {
    ?>
    <ul>
        <li>
            Categorie : <?= $categorie->libelle ?> 
            <form style="display:inline" action="<?= $router->generateURL("categorie.modifier"); ?>&id=<?= $categorie->id ?>" method="post">
                <?= $form->submit("submit", "Modifier"); ?>
            </form>
            <form style="display:inline" action="<?= $router->generateURL("categorie.supprimer"); ?>" method="post">
                <?= $form->input("libelle", "", ["type" => "hidden", "value" => $categorie->libelle, "noLabel" => true]); ?>
                <?= $form->input("id", "", ["type" => "hidden", "value" => $categorie->id, "noLabel" => true]); ?>
                <?= $form->submit("submit", "Supprimer"); ?>
            </form>            
        </li>
    </ul>
    <?Php
}
?>

<hr />

<?php
$form->surround = "div";
require_once $router->routes["categorie.ajouter"]["vue"];
?>