<?Php
$controleur->modifierCategorie($flash );
$categorie = $controleur->lireCategorie();
?>
<form action="<?= $router->generateURL("categorie.modifier"); ?>&id=<?= $categorie->id ?>" method="post">
    <?= $form->input("libelle", "Libelle : ", ["value" => $categorie->libelle]); ?>
    <?= $form->input("id", "", ["type" => "hidden", "value" => $categorie->id, "noLabel" => true]); ?>
    <?= $form->submit("submit", "Modifier"); ?>
</form>