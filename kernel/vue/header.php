<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Teddy Beer</title>
    <link rel="stylesheet" href="asset/css/bootstrap-4.0.0-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/style.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
    <a class="navbar-brand" href="<?=$router->generateURL()?>">Teddy Beer</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" ><a class='nav-link <?= $router->is_actual("biere.view") ? "active" :"" ?>'  href="<?= $router->generateURL() ?>">Consulter les bières</a></li>
            <li class="nav-item" ><a class="nav-link <?= $router->is_actual("categorie.view") ? "active" :"" ?>" href="<?= $router->generateURL("categorie.view") ?>">Consulter les catégories</a></li>
            <?php if (!isset($_SESSION['user'])) { ?>
                <li class="nav-item"><a class="nav-link <?= $router->is_actual("user.connexion") ? "active" :"" ?>" href="<?= $router->generateURL("user.connexion") ?>">Connexion</a></li>
                <li class="nav-item"><a class="nav-link <?= $router->is_actual("user.inscription") ? "active" :"" ?>" href="<?= $router->generateURL("user.inscription") ?>">Inscription</a></li> 
            <?Php
                } else {
                    echo '<li class="nav-item"><a class="nav-link" href="./deco.php">Déco</a></li>';
                }
            ?>
        </ul>
        </div>
    </nav>
    <div class="container">
<?Php
if (!is_null($flash->get("success"))) {
    echo '<div class="alert alert-success" role="alert">'.$flash->get('success').'</div>';
}
if (!is_null($flash->get("error"))) {
    echo '<div class="alert alert-danger" role="alert">'.$flash->get('error').'</div>';
}
?>
</div>